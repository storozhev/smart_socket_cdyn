use std::ffi::{c_char, c_void, CString};

#[derive(Default, Debug)]
#[repr(C)]
pub struct Socket {
    power: u32,
    turned_on: bool,
}

impl Socket {
    fn turn_on(&mut self) {
        self.power = 220;
        self.turned_on = true;
    }

    fn turn_off(&mut self) {
        self.power = 0;
        self.turned_on = false;
    }

    fn info(&self) -> String {
        format!("turned_on:{}, power:{}", self.turned_on, self.power)
    }
}

#[repr(transparent)]
pub struct SocketPtr(*const c_void);

impl From<SocketPtr> for &'static mut Socket {
    fn from(socket_ptr: SocketPtr) -> Self {
        unsafe { &mut *(socket_ptr.0 as *mut _) }
    }
}

#[repr(transparent)]
pub struct SocketInfoPtr(*const c_char);

pub type NewFn = unsafe extern "C" fn(*mut SocketPtr);
pub type TurnOnFn = unsafe extern "C" fn(SocketPtr);
pub type TurnOffFn = unsafe extern "C" fn(SocketPtr);
pub type InfoFn = unsafe extern "C" fn(SocketPtr) -> SocketInfoPtr;

pub unsafe extern "C" fn socket_new(socket_ptr: *mut SocketPtr) {
    let socket = Socket::default();

    *socket_ptr = SocketPtr(Box::leak(Box::new(socket)) as *const _ as *const c_void);
}

pub unsafe extern "C" fn socket_turn_on(socket_ptr: SocketPtr) {
    let socket: &mut Socket = socket_ptr.into();
    socket.turn_on();
}

pub unsafe extern "C" fn socket_turn_off(socket_ptr: SocketPtr) {
    let socket: &mut Socket = socket_ptr.into();
    socket.turn_off();
}

pub unsafe extern "C" fn socket_info(socket_ptr: SocketPtr) -> SocketInfoPtr {
    let socket = &*(socket_ptr.0 as *const Socket);
    let info = CString::new(socket.info().as_str()).unwrap();

    SocketInfoPtr(Box::leak(Box::new(info)).as_ptr())
}
