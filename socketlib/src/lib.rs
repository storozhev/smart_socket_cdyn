mod socket;
use socket::*;

#[no_mangle]
pub extern "C" fn get_lib_functions() -> LibFunctions {
    LibFunctions::default()
}

#[repr(C)]
pub struct LibFunctions {
    new: NewFn,
    turn_on: TurnOnFn,
    turn_off: TurnOffFn,
    info: InfoFn,
}

impl Default for LibFunctions {
    fn default() -> Self {
        Self {
            new: socket_new,
            turn_on: socket_turn_on,
            turn_off: socket_turn_off,
            info: socket_info,
        }
    }
}
