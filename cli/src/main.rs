mod socketlib;
use socketlib::SocketLib;
use std::error::Error;
use std::io;

fn main() -> Result<(), Box<dyn Error>> {
    let socket = SocketLib::new();

    loop {
        let mut buf = String::new();
        io::stdin().read_line(&mut buf)?;

        match buf.trim_end() {
            "on" => socket.turn_on(),
            "off" => socket.turn_off(),
            "info" => println!("info:{}", socket.info()),
            _ => continue,
        }
    }
}
