use libloading::{Library, Symbol};
use std::ffi::{c_char, c_void, CStr};

const LIB_FILE: &str = "target/release/libsocketlib.so";

pub struct SocketLib {
    lib_functions: LibFunctions,
    _lib: Library,
    socket_ptr: SocketPtr,
}

impl SocketLib {
    pub fn new() -> Self {
        let lib = unsafe { Library::new(LIB_FILE).expect("failed to load library") };
        let get_lib_functions: Symbol<GetLibFn> = unsafe {
            lib.get(b"get_lib_functions")
                .expect("failed to load symbol")
        };

        let mut socket_ptr = SocketPtr(std::ptr::null_mut());
        let lib_functions = unsafe { get_lib_functions() };

        unsafe { (lib_functions.new)(&mut socket_ptr) };

        Self {
            lib_functions,
            _lib: lib,
            socket_ptr,
        }
    }

    pub fn turn_on(&self) {
        unsafe { (self.lib_functions.turn_on)(self.socket_ptr) };
    }

    pub fn turn_off(&self) {
        unsafe { (self.lib_functions.turn_off)(self.socket_ptr) };
    }

    pub fn info(&self) -> String {
        let socket_info_ptr = unsafe { (self.lib_functions.info)(self.socket_ptr) };
        let cstr = unsafe { CStr::from_ptr(socket_info_ptr.0) };

        cstr.to_str().unwrap().to_string()
    }
}

impl Drop for SocketLib {
    fn drop(&mut self) {
        unsafe { Box::from_raw(self.socket_ptr.0) };
    }
}

type GetLibFn = unsafe extern "C" fn() -> LibFunctions;

#[derive(Debug)]
#[repr(C)]
struct LibFunctions {
    new: NewFn,
    turn_on: TurnOnFn,
    turn_off: TurnOffFn,
    info: InfoFn,
}

#[derive(Debug, Clone, Copy)]
#[repr(transparent)]
struct SocketPtr(*mut c_void);

#[repr(transparent)]
struct SocketInfoPtr(*const c_char);

type NewFn = unsafe extern "C" fn(*mut SocketPtr);
type TurnOnFn = unsafe extern "C" fn(SocketPtr);
type TurnOffFn = unsafe extern "C" fn(SocketPtr);
type InfoFn = unsafe extern "C" fn(SocketPtr) -> SocketInfoPtr;
